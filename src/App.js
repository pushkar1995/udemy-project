import React, { Component } from 'react';
import Radium, { StyleRoot } from 'radium';
import './App.css';

import Person from './Person/Person';

class App extends Component {
  state = {
    persons: [
      { id: 'jkasd', name: 'Max', age:28 },
      { id: 'jka', name: 'Manu', age:29 },
      { id: 'jkas', name: 'Stephanie', age:26}
    ],
    otherState: 'some other value',
    showPersons: false
  }

  // switchNameHandler = (newName) => {
  //   //console.log('Was clicked!');
  //   this.setState( {
  //     persons: [
  //       { name: newName, age:28 },
  //       { name: 'Manu', age:29 },
  //       { name: 'Stephanie', age:28 }
  //     ]
  //   } )
  // }


  nameChangedHandler = ( event, id ) => {
    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });

    const person = {
        ...this.state.persons[personIndex]
    };

    person.name = event.target.value;

    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState( {
      persons: persons
      // [
      //   { name: 'Max', age:28 },
      //   { name: event.target.value, age:29 },
      //   { name: 'Stephanie', age:28 }
      // ]
    } )
  }



  tooglePersonHandler = () => {
    const doesShow = this.state.showPersons;
    this.setState({showPersons: !doesShow});
  }

  deletePersonHandler = (personIndex) => {
    // OR   const persons = this.state.persons.slice();
    const persons = [...this.state.persons]; //OR ES6 more reliable way using Spread OPERATOR
    persons.splice(personIndex, 1);
    this.setState({persons: persons})
  }
  

  render() {

    const style = {
      backgroundColor: 'green',
      color: 'white',
      font: 'inherit',
      border: '1px solid blue',
      padding: '8px',
      cursor: 'pointer',
      ':hover' : {
        backgroundColor: 'lightgreen',
        color: 'black'
      }
    };

    let persons = null;

    if (this.state.showPersons) {
      persons = (
        <div>
          {this.state.persons.map((person, index )=> {
            return <Person 
                        name={person.name} 
                        age={person.age}
                        click={() => this.deletePersonHandler(index)}
                        key={person.id}
                        changed={(event) => this.nameChangedHandler(event, person.id)} />
          })}
        {/* <Person 
            name={this.state.persons[0].name} 
            age={this.state.persons[0].age}/>
        <Person 
            name={this.state.persons[1].name} 
            age={this.state.persons[1].age}
            click={this.switchNameHandler.bind(this, 'Makadi')}
            changed={this.nameChangedHandler}>My Hobbies: Sleeping</Person>
        <Person 
            name={this.state.persons[2].name} 
            age={this.state.persons[2].age}/> */}
      </div> 
      );

      style.backgroundColor = 'red';
      style[ ':hover' ] = {
        backgroundColor: 'lightgreen',
        color: 'black'
      };
    }

    //let classes = ['red', 'bold'].join(' ');
        // OR
    const classes = [];
    if (this.state.persons.length <= 2) {
      classes.push('red'); //classes = ['red']
    }
    if (this.state.persons.length <= 1) {
      classes.push('bold'); //classes = ['red', 'bold']
    }

    return (
      <StyleRoot>
      <div className="App">
       <h1>I am a react app</h1>
       <p className={classes.join(' ')}>This is really working</p>
       <button 
          onClick={this.tooglePersonHandler}
          style={style}>Toggle Persons</button>
       {/* {this.state.showPersons === true ? */}
        {/* : null } */}
        {persons}
       
      </div>
      </StyleRoot>
    );
  }
}

export default Radium(App);
